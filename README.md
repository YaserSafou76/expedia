#SIMPLE HOTELS OFFERS SEARCH 
this project consumes response from API service call and view results 
in readable way

#REQUIRMENTS
- JAVA 8 JDK
- MAVEN
- Apache Tomcat 8.5 or later 

#how to deploy  
-assuming you have java jdk 1.8 installed in your system if not 
you may find it 
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

-make sure that JAVA-HOME is set in your System

-you can either download or clone the project into your local machine 
or you may just run this command in your terminal
$git clone https://YaserSafou76@bitbucket.org/YaserSafou76/expedia.git

-on project root run command
mvn install

-now the project folder contains the needed war file to deploy

-just drop the war file into tomcat CATALINA base folder and enjoy

#note 
you may run it using eclipse by assigning server to the project and run. 
https://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.stardust.docs.wst%2Fhtml%2Fwst-integration%2Fconfiguration.html

-a working copy of this project can be viewed in 
https://hotels-deals-yaser.herokuapp.com


 
